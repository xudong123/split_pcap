# Split Pcap #

This repository is used to split pcap file by session.

### What is this repository for? ###

* Split a pcap file to many pcap files by session.
* V 0.1

### How do I get set up? ###

* depends libpcap
* git clone this repo
* ./configure
* make
* make install (use root)