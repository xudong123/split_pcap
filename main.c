#include "split_pcap.h"
#include <net/ethernet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>


char errbuf[PCAP_ERRBUF_SIZE];

void
myCallback(u_char *user,
	   const struct pcap_pkthdr *h,
	   const u_char *bytes)
{
  struct ether_header *p_eheader = NULL;
  struct ip *p_ipheader = NULL;
  struct tcphdr *p_tcpheader = NULL;
  char *ip_src = NULL, *ip_dst = NULL;

  p_eheader = (struct ether_header *)bytes;
  p_ipheader = (struct ip *)(bytes + ETHER_HDR_LEN);
  p_tcpheader = (struct tcphdr *)(bytes + ETHER_HDR_LEN + p_ipheader->ip_hl * 4);
  ip_src = strdup(inet_ntoa(p_ipheader->ip_src));
  ip_dst = strdup(inet_ntoa(p_ipheader->ip_dst));

  printf("src ip: %s:%d, dst ip: %s:%d\n", ip_src,
	 ntohs(p_tcpheader->th_sport),
	 ip_dst,
	 ntohs(p_tcpheader->th_dport));
  free(ip_src);
  free(ip_dst);
  ip_src = NULL;
  ip_dst = NULL;
}


int
main(int argc, char* argvp[]) {
  pcap_t *pt = NULL;

  pt = pcap_open_offline("test.pcap", errbuf);
  if (pt == NULL) {
    printf("error in pcap open offline.\n");
    return 1;
  }

  pcap_loop(pt, -1, myCallback, NULL);
  
  return 0;
}
